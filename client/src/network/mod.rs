// pub mod worker;

use std::net::TcpStream;
use std::sync::Mutex;

use bevy::app::{AppBuilder, Plugin};
use crossbeam::Receiver;
use crossbeam::Sender;

use bevy::prelude::*;
use common::network::{Command, StreamHandler};
use common::{ClientToServer, ServerToClient};

// #[derive(Debug)]
// struct NetworkEvent(ServerToClient);
pub type NetworkEvent = ServerToClient;
pub type NetworkMessage = ClientToServer;

pub struct NetworkResource {
    event_rx: Mutex<Receiver<NetworkEvent>>,
    message_tx: Mutex<Sender<NetworkMessage>>,
    instruction_tx: Mutex<Sender<Command>>,
}

pub struct NetworkingPlugin;

impl Plugin for NetworkingPlugin {
    fn build(&self, app: &mut AppBuilder) {
        let stream = TcpStream::connect("localhost:9001").unwrap();
        let mut handler = StreamHandler::<ServerToClient, ClientToServer>::new(stream);

        let resource = NetworkResource {
            event_rx: Mutex::new(handler.get_message_receiver()),
            message_tx: Mutex::new(handler.get_packet_sender()),
            instruction_tx: Mutex::new(handler.get_command_sender()),
        };

        std::thread::spawn(move || handler.run());

        app.add_event::<NetworkEvent>()
            .add_resource(resource)
            .add_system(process_network_events.system());
    }
}

fn process_network_events(
    net: ResMut<NetworkResource>,
    mut network_events: ResMut<Events<NetworkEvent>>,
) {
    let locked = match net.event_rx.lock() {
        Ok(l) => l,
        // this system is the only consumer of `event_rx`, so if this lock is poisoned that means
        // a previous iteration of our thread panic'd without taking down the game. We'll
        // bravely try and soldier on and continue to process network event's, but it's pretty
        // bad.
        Err(p) => p.into_inner(),
    };

    while let Ok(event) = locked.try_recv() {
        network_events.send(event);
    }
}

impl NetworkResource {
    pub fn send(&self, message: NetworkMessage) -> Result<(), std::io::Error> {
        self.message_tx.lock().unwrap().send(message).unwrap();
        Ok(())
    }
}

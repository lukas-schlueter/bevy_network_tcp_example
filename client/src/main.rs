use crate::network::{NetworkEvent, NetworkMessage, NetworkResource};
use bevy::app::ScheduleRunnerPlugin;
use bevy::prelude::*;
use std::time::Duration;

mod network;

fn main() {
    App::build()
        .add_plugin(bevy::type_registry::TypeRegistryPlugin::default())
        .add_plugin(bevy::core::CorePlugin)
        .add_plugin(ScheduleRunnerPlugin::run_loop(Duration::from_secs_f64(
            1.0 / 60.0,
        )))
        .add_plugin(network::NetworkingPlugin)
        .init_resource::<NetworkEventReader>()
        .add_system(greeter.system())
        .add_system(printer.system())
        .run();
}

fn greeter(net: ResMut<NetworkResource>) {
    let msg = NetworkMessage::Greet("World".to_string(), 15);
    println!("---> {:?}", &msg);
    net.send(msg).unwrap();
}

#[derive(Default)]
struct NetworkEventReader {
    network_events: EventReader<NetworkEvent>,
}

fn printer(mut state: ResMut<NetworkEventReader>, events: Res<Events<NetworkEvent>>) {
    for event in state.network_events.iter(&events) {
        println!("<--- {:?}", event);
    }
}

use std::net::{TcpListener, TcpStream};
use std::thread;
use std::thread::spawn;

use serde::Deserialize;
use serde::Serialize;

use common::network::{Command, StreamHandler};
use std::time::Duration;

fn main() {
    let server = thread::spawn(|| server());
    let client = thread::spawn(|| client());

    client.join();
    server.join(); // Never happens
}

fn client() {
    let mut stream = TcpStream::connect("localhost:9001").unwrap();
    let mut handler = StreamHandler::<ToClient, ToServer>::new(stream);

    let incoming = handler.get_message_receiver();
    let outgoing = handler.get_packet_sender();
    let commands = handler.get_command_sender();

    spawn(move || handler.run());

    for i in 0..10 {
        let mut msgs = incoming.try_iter();
        msgs.for_each(|msg| {
            dbg!("Received a message: {:?}", &msg);
        });

        outgoing
            .send(ToServer::Greeting {
                name: "World".to_string(),
                age: i,
            })
            .unwrap();
        thread::sleep(Duration::from_millis(500));
    }

    commands.send(Command::SHUTDOWN).unwrap();
}

#[derive(Deserialize, Serialize, Debug)]
enum ToServer {
    Greeting { name: String, age: usize },
}

#[derive(Deserialize, Serialize, Debug)]
enum ToClient {
    Welcome { message: String },
}

// Server never shuts down!
fn server() {
    println!("Welcome to server!");
    let listener = TcpListener::bind("0.0.0.0:9001").unwrap();

    println!("Listener created!");

    for stream in listener.incoming() {
        println!("New connection!");
        spawn(move || {
            let stream = stream.expect("Failed to create tcp connection");
            let mut handler = StreamHandler::<ToServer, ToClient>::new(stream);

            let incoming = handler.get_message_receiver();
            let outgoing = handler.get_packet_sender();
            let _commands = handler.get_command_sender();

            spawn(move || handler.run());

            loop {
                if let Ok(msg) = incoming.try_recv() {
                    match msg {
                        ToServer::Greeting { name, age } => {
                            dbg!("Received greeting: name: {}, age: {}", &name, &age);
                            outgoing
                                .send(ToClient::Welcome {
                                    message: format!("Welcome {} with age {}", &name, &age),
                                })
                                .unwrap();
                        }
                    }
                }
            }
        });
    }
    println!("Server is done!");
}

use serde::{Deserialize, Serialize};

pub mod network;

/// For use in `client` and `server`
#[derive(Deserialize, Serialize, Debug)]
pub enum ClientToServer {
    Greet(String, usize),
}

/// For use in `client` and `server`

#[derive(Deserialize, Serialize, Debug)]
pub enum ServerToClient {
    Greet(String),
}

use crossbeam::{Receiver, Sender};

pub use stream::Command;
pub use stream::StreamHandler;

mod parser;
mod stream;

pub(crate) struct Channel<T> {
    sender: Sender<T>,
    receiver: Receiver<T>,
}

impl<T> From<(Sender<T>, Receiver<T>)> for Channel<T> {
    fn from(from: (Sender<T>, Receiver<T>)) -> Self {
        Self {
            sender: from.0,
            receiver: from.1,
        }
    }
}

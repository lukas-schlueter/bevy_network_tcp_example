use core::marker::PhantomData;

use byteorder::{BigEndian, ByteOrder};
use serde::de::DeserializeOwned;
use thiserror::Error;

use crate::network::parser::ParseError::*;

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("data exceeds maximum size.")]
    MessageTooLong,
    #[error("data does not contain '\n' and does not exceed maximum size.")]
    NotReady,
    #[error("failed to deserialize message from data")]
    DeserializationError(#[from] rmp_serde::decode::Error),
}

#[derive(Debug)]
pub struct Parser<T: DeserializeOwned> {
    to_read: Option<usize>,
    data: Vec<u8>,
    _phantom_data: std::marker::PhantomData<T>,
}

impl<T: DeserializeOwned> Parser<T> {
    pub fn new() -> Self {
        Self {
            to_read: None,
            data: vec![],
            _phantom_data: PhantomData,
        }
    }

    pub fn push_and_parse(&mut self, buf: &[u8]) -> Result<Vec<T>, ParseError> {
        self.data.extend_from_slice(buf);

        let mut results = vec![];
        loop {
            match self.parse_data() {
                Ok(msg) => results.push(msg),
                Err(NotReady) => break,
                Err(e) => return Err(e),
            }
        }
        Ok(results)
    }

    // TODO: Consider rewriting to use &[u8] instead of Vec<u8>
    fn parse_data(&mut self) -> Result<T, ParseError> {
        if self.to_read.is_none() {
            if self.data.len() < 2 {
                return Err(NotReady);
            }
            self.to_read = Some(<BigEndian as ByteOrder>::read_u16(&self.data[0..2]) as usize);
            self.data = self.data[2..].to_vec();
        }

        let to_read = self
            .to_read
            .expect("Fatal error! Something really weird just happened!");

        if self.data.len() >= (to_read) as usize {
            return match rmp_serde::from_read_ref(&self.data[..to_read]) {
                Ok(msg) => {
                    self.data = self.data[to_read..].to_vec();
                    self.to_read = None;
                    Ok(msg)
                }
                Err(e) => Err(DeserializationError(e)),
            };
        }
        if self.data.len() > 8192 as usize {
            return Err(MessageTooLong);
        }
        Err(NotReady)
    }
}

#[cfg(test)]
mod tests {
    use byteorder::BigEndian;
    use serde::export::PhantomData;
    use serde::Deserialize;
    use serde::Serialize;

    use crate::network::parser::Parser;

    use super::*;

    #[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
    struct TestMsg {
        name: String,
        last_name: String,
        age: u8,
    }

    #[test]
    fn test_parse_data() {
        let test_struct = TestMsg {
            name: "Hello".to_string(),
            last_name: "Test".to_string(),
            age: 193,
        };

        let mut serialized = rmp_serde::to_vec(&test_struct).unwrap();
        let mut buf = [0; 2];
        let mut data = Vec::with_capacity(2 + serialized.len());
        BigEndian::write_u16(&mut buf, serialized.len() as u16);
        data.extend_from_slice(&buf);
        data.append(&mut serialized);

        let mut parser = Parser {
            to_read: None,
            data,
            _phantom_data: PhantomData,
        };

        let msg = parser.parse_data().unwrap();
        assert_eq!(test_struct, msg);
        assert_eq!(parser.to_read, None);
    }

    #[test]
    fn test_parse_data_multiple() {
        let mut msgs = vec![];
        let mut data = vec![];
        for i in 0..20 {
            let msg = TestMsg {
                name: "Testy".to_string(),
                last_name: "McTestface".to_string(),
                age: i,
            };
            let mut serialized = rmp_serde::to_vec(&msg).unwrap();
            let mut buf = [0; 2];
            BigEndian::write_u16(&mut buf, serialized.len() as u16);
            data.extend_from_slice(&buf);
            data.append(&mut serialized);
            msgs.push(msg);
        }

        println!("Data is {} bytes: {:?}", data.len(), data);
        let mut parser = Parser::<TestMsg>::new();

        let parsed_msgs = parser.push_and_parse(&data[..1]).unwrap();
        assert_eq!(0, parsed_msgs.len());

        let parsed_msgs = parser.push_and_parse(&data[1..5]).unwrap();
        assert_eq!(0, parsed_msgs.len());

        let parsed_msgs = parser.push_and_parse(&data[5..]).unwrap();

        assert_eq!(msgs, parsed_msgs);
        assert_eq!(None, parser.to_read);
        assert_eq!(0, parser.data.len());
    }
}

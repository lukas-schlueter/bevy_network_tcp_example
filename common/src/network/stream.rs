use byteorder::BigEndian;
use byteorder::WriteBytesExt;
use core::time::Duration;
use crossbeam::{Receiver, Sender};
use serde::de::DeserializeOwned;
use serde::ser::Serialize;
use std::io::Read;
use std::io::Write;
use std::net::Shutdown;
use std::net::TcpStream;
use std::time::Instant;

use crate::network::parser::{ParseError, Parser};
use crate::network::Channel;

pub enum Command {
    SHUTDOWN,
}

pub struct StreamHandler<I: DeserializeOwned, O: Serialize> {
    messages: Channel<I>,
    packets: Channel<O>,
    commands: Channel<Command>,
    stream: TcpStream,
    parser: Parser<I>,
}

impl<I: DeserializeOwned, O: Serialize> StreamHandler<I, O> {
    pub fn new(stream: TcpStream) -> Self {
        stream.set_nonblocking(true).unwrap();

        Self {
            messages: crossbeam::unbounded().into(),
            packets: crossbeam::unbounded().into(),
            commands: crossbeam::unbounded().into(),
            stream,
            parser: Parser::new(),
        }
    }

    pub fn get_packet_sender(&self) -> Sender<O> {
        self.packets.sender.clone()
    }

    pub fn get_message_receiver(&self) -> Receiver<I> {
        self.messages.receiver.clone()
    }

    pub fn get_command_sender(&self) -> Sender<Command> {
        self.commands.sender.clone()
    }

    pub fn run(&mut self) {
        self.run_with_duration(Duration::from_millis(1))
    }

    pub fn run_with_duration(&mut self, sleep_time: Duration) {
        let mut start = Instant::now();
        let mut end = Instant::now();

        loop {
            let millis = start.elapsed().as_millis();
            if millis > 50 {
                println!(
                    "warning: thread worker loop took {:.3?} ({:.3?} after sleeping)",
                    start.elapsed(),
                    end.elapsed()
                );
            }

            start = Instant::now();

            if self.handle_commands() {
                // TODO: Shutdown the socket!
                break;
            }

            self.send_packets();
            self.receive_messages();

            end = Instant::now();

            std::thread::sleep(sleep_time);
        }
    }

    fn handle_commands(&mut self) -> bool {
        while let Ok(command) = self.commands.receiver.try_recv() {
            match command {
                Command::SHUTDOWN => {
                    println!("Shutting down!");
                    self.stream.shutdown(Shutdown::Both).unwrap();
                    return true;
                }
            }
        }
        false
    }

    fn send_packets(&mut self) {
        while let Ok(packet) = self.packets.receiver.try_recv() {
            self.send_packet(packet).unwrap();
        }
    }

    fn send_packet<T: Serialize>(&mut self, msg: T) -> Result<(), std::io::Error> {
        let stream = &mut self.stream;
        let serialized = rmp_serde::to_vec(&msg).unwrap();
        stream.write_u16::<BigEndian>(serialized.len() as u16)?;
        stream.write_all(&serialized)?;
        Ok(())
    }

    fn receive_messages(&mut self) {
        let mut buf = [0; 256];

        loop {
            match self.stream.read(&mut buf) {
                Ok(size) => match self.parser.push_and_parse(&buf[..size]) {
                    Ok(msgs) => {
                        for msg in msgs {
                            self.messages.sender.send(msg).unwrap();
                        }
                    }
                    Err(ParseError::NotReady) => continue,
                    Err(e) => panic!("Failed to parse data: {:?}", e),
                },
                Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => break,
                Err(e) => panic!("Failed to read data: {:?}", e),
            };
        }
    }
}

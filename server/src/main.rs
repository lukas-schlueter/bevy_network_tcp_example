use common::network::StreamHandler;
use common::{ClientToServer, ServerToClient};
use std::net::TcpListener;
use std::thread::spawn;

// Server never quits
fn main() {
    println!("Welcome to server!");
    let listener = TcpListener::bind("0.0.0.0:9001").unwrap();

    println!("Listener created!");

    for stream in listener.incoming() {
        println!("New connection!");
        spawn(move || {
            let stream = stream.expect("Failed to create tcp connection");
            let mut handler = StreamHandler::<ClientToServer, ServerToClient>::new(stream);

            let incoming = handler.get_message_receiver();
            let outgoing = handler.get_packet_sender();
            let _commands = handler.get_command_sender();

            spawn(move || handler.run());

            loop {
                if let Ok(msg) = incoming.try_recv() {
                    match msg {
                        ClientToServer::Greet(name, age) => {
                            dbg!("Received greeting: name: {}, age: {}", &name, &age);
                            outgoing
                                .send(ServerToClient::Greet(format!(
                                    "Welcome {} with age {}",
                                    &name, &age
                                )))
                                .unwrap();
                        }
                    }
                }
            }
        });
    }
    println!("Server is done!");
}
